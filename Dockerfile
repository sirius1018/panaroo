FROM continuumio/miniconda3:4.10.3p0
RUN apt-get update
RUN conda config --add channels bioconda --add channels conda-forge
RUN conda install -y -c conda-forge mamba
RUN mamba install -y -c conda-forge -c bioconda -c defaults panaroo
RUN echo 'alias ll="ls -all"' >> ~/.bashrc
